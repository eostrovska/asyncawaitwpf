﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait
{
    class MyClass
    {
        double Operation(object argument)
        {
            CancellationToken token = (CancellationToken)argument;

            Thread.Sleep(2000);
            token.ThrowIfCancellationRequested();
            return (double)argument * (double)argument;
        }

        public async Task<double> OperationAsync(double argument)
        {
            return await Task<double>.Factory.StartNew(Operation, argument);
        }
    }

    class Program
    {
        static void Main()
        {
            MyClass my = new MyClass();
            Task<double> task = my.OperationAsync(3);
            Action<Task<double>> continuation;

            continuation = t => Console.WriteLine("Result : " + task.Result);
            task.ContinueWith(continuation, TaskContinuationOptions.OnlyOnRanToCompletion);

            continuation = t => Console.WriteLine("Inner Exception : " + task.Exception.InnerException.Message);
            task.ContinueWith(continuation, TaskContinuationOptions.OnlyOnFaulted);

            Console.ReadKey();
        }
    }
}
