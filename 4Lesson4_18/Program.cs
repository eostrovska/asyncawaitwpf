﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _4Lesson4_18
{
    class Program
    {
        static volatile int counter;

        static void Main(string[] args)
        {
            CancellationTokenSource cancellation = new CancellationTokenSource();
            int[] array = new int[10000000];

            Parallel.For(0, 10000000, (i) => {

                array[i] = new Random().Next();

                if (array[i] % 2 == 0)
                {
                    counter++;
                    Console.WriteLine("");
                }
            });

            ParallelQuery<int> odds = from element in array
                                               .AsParallel()
                                               .WithCancellation(cancellation.Token)
                                           where element % 2 == 0
                                           select element;

            cancellation.CancelAfter(100000);
            int oddLenght = odds.Count();

            foreach (int element in odds)
                Console.Write(element + " ");

            Console.WriteLine("Lenght is {0}", oddLenght);
            Console.WriteLine("Counter is {0}", counter);
            Console.ReadKey();
        }
    }
}
