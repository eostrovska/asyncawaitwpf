﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AsyncAwaitWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private async void button_Click_1(object sender, RoutedEventArgs e)
        {
            Task<double> task = new Task<double>(Operation);
            task.Start();
            await task;
            label.Content = task.Result.ToString();
        }

        double Operation()
        {
            Thread.Sleep(2000);
            double one = 1000;
            double two = 2345;
            return one * two;
        }
    }
}
